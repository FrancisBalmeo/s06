let favorite_food = "Nuggets";
let sum = 150 + 9;
let product = 100 * 90;
let isActive = true;

let favorite_restaurants = ["Shakey's", "Max's", "Ken Rogers", "Kuya J", "AllForU"];
let favorite_singer ={
firstName: "MinJeong",
lastName: "Kim",
stageName: "Winter",
birthday: "January 1, 2001",
age: "21",
bestAlbum: "Savage, 2021",
bestSong: "Savage",
isActive: true
};

console.log(favorite_food);
console.log(sum);
console.log(product);
console.log(isActive);
console.log(favorite_restaurants);
console.log(favorite_singer);

function divideTwoNumbers(x, y){
	return x / y;
}

let quotient = divideTwoNumbers(10,5);

console.log(`The result of the division is ${quotient}`);